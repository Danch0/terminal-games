import keyboard
import threading
import time
import subprocess as sp
import random

class Thread (threading.Thread):
    def __init__(self, threadID, name, func, delay, run_event, level, enemy, player):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.func = func
        self.delay = delay
        self.stop = False
        self.run_event = run_event
        self.level = level
        self.enemy = enemy
        self.player = player

    def run(self):
        #print("Starting " + self.name)
        self.func(self.enemy, self.level, self.name, self.delay, self.run_event, self.player)
        print("\nExiting " + self.name)

class Level:
    def __init__(self, w, h):
        self.w = w
        self.h = h
        self.canvas = self.setCanvas(1)

    def setCanvas(self, mid_setter):
        # sky
        sky = ''
        for c in range(self.w):
            sky += ' '

        # horizon
        horizon = ''
        for c in range(self.w):
            horizon += '_'

        # track
        track = ''
        for c in range(2, self.h):
            track_row = ''
            for i in range(self.w):
                track_row += ' '
            middle_lane_index = int(self.w / 2)
            left_lane_index = self.h - c - 1
            right_lane_index = middle_lane_index + c + 1
            track_row = list(track_row)
            track_row[left_lane_index] = '/'
            if c % 2 == mid_setter:
                track_row[middle_lane_index] = '|'
            track_row[right_lane_index] = '\\'
            track_row = ''.join(track_row)
            track += '\n' + track_row

        canvas = '\n' + sky + '\n' + horizon + track

        return canvas

class Player:
    def __init__(self, right): # right is boolean
        self.alive = True
        self.right = right
        self.left_image = '[/|]\n /o|\n[/__|]'
        self.right_image = '[|\]\n |o\\\n[|__\]'

    def move(self):
        if keyboard.is_pressed('right'):
            self.right = True
        elif keyboard.is_pressed('left'):
            self.right = False

class Enemy:
    def __init__(self, frame_count, right, level):
        self.right = right
        self.frame_count = frame_count
        # frame 1
        self.frame1 = ['/', '\\']
        self.frame1_indexes = [2 * level.w + int(level.w / 2) + 2, 2 * level.w + int(level.w / 2) + 4]
        # frame 2
        self.frame2 = ['^\n|/', '^\n\|']
        self.frame2_indexes = [2 * level.w + int(level.w / 2) + 2, 2 * level.w + int(level.w / 2) + 4]
        # frame 3
        self.frame3 = ['__\n|/>', ' __\n<\|']
        self.frame3_indexes = [3 * level.w + int(level.w / 2) + 2, 3 * level.w + int(level.w / 2) + 5]
        #frame 4
        self.frame4 = ['__\n|*/\n<|/>', '__\n\*|\n<\|>']
        self.frame4_indexes = [3 * level.w + int(level.w / 2) + 2, 3 * level.w + int(level.w / 2) + 5]
        # frame 5
        self.frame5 = ['__\n|*/>\n<|/>', ' __\n<\*|\n <\|>']
        self.frame5_indexes = [4 * level.w + int(level.w / 2) + 2, 4 * level.w + int(level.w / 2) + 6]
        # frame 6
        self.frame6 = [' __\n<|*/>\n <|/>', ' __\n<\*|>\n <\|>']
        self.frame6_indexes = [5 * level.w + int(level.w / 2) + 2, 5 * level.w + int(level.w / 2) + 7]
        # frame 7
        self.frame7 = [' ___\n<|* />\n <|_/>', ' ___\n<\ *|>\n <\_|>']
        self.frame7_indexes = [6 * level.w + int(level.w / 2) + 2, 6 * level.w + int(level.w / 2) + 8]
        # frame 8
        self.frame8 = [' ____\n<| * />\n <|__/>', ' ____\n<\ * |>\n <\__|>']
        self.frame8_indexes = [7 * level.w + int(level.w / 2) + 2, 7 * level.w + int(level.w / 2) + 9]
        # frame 9
        self.frame9 = [' _____\n<| *  />', ' _____\n<\  * |>']
        self.frame9_indexes = [8 * level.w + int(level.w / 2) + 2, 8 * level.w + int(level.w / 2) + 10]
        # frame 10
        self.frame10 = ['_______', ' _______']
        self.frame10_indexes = [9 * level.w + int(level.w / 2) - 8, 9 * level.w + int(level.w / 2) + 4]

def hitCheck(enemy, player):
    if player.right == enemy.right:
        player.alive = False

def drawEnemy(enemy, level, delay, player):
    frame_counter = 0
    while True:
        # clear the previous enemy frame to draw the next frame
        level.canvas = ''
        level.canvas = level.setCanvas(frame_counter % 2)

        #print(frame_counter, frame_counter % 10 == 0, frame_counter % 10 == 1, frame_counter % 10 == 2)
        level_view = level.canvas
        level_view = list(level_view)
        if frame_counter % 10 == 0:
            # draw enemy at frame 1
            if enemy.right:
                frame1 = enemy.frame1[1]
                index1 = enemy.frame1_indexes[1]
                level_view[index1] = frame1
            else:
                frame1 = enemy.frame1[0]
                index1 = enemy.frame1_indexes[0]
                level_view[index1] = frame1
                level_view = ''.join(level_view)
        elif frame_counter % 10 == 1:
            # draw enemy at frame 2
            if enemy.right:
                frame2 = enemy.frame2[1]
                index2 = enemy.frame2_indexes[1]
                enemy_parts = frame2.split('\n')
                for part in enemy_parts:
                    part_index = index2 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame2 = enemy.frame2[0]
                index2 = enemy.frame2_indexes[0]
                enemy_parts = frame2.split('\n')
                for part in enemy_parts:
                    part_index = index2 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
        elif frame_counter % 10 == 2:
            # draw enemy at frame 3
            if enemy.right:
                frame3 = enemy.frame3[1]
                index3 = enemy.frame3_indexes[1]
                enemy_parts = frame3.split('\n')
                for part in enemy_parts:
                    part_index = index3 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame3 = enemy.frame3[0]
                index3 = enemy.frame3_indexes[0]
                enemy_parts = frame3.split('\n')
                for part in enemy_parts:
                    part_index = index3 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
        elif frame_counter % 10 == 3:
            # draw enemy at frame 4
            if enemy.right:
                frame4 = enemy.frame4[1]
                index4 = enemy.frame4_indexes[1]
                enemy_parts = frame4.split('\n')
                for part in enemy_parts:
                    part_index = index4 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame4 = enemy.frame4[0]
                index4 = enemy.frame4_indexes[0]
                enemy_parts = frame4.split('\n')
                for part in enemy_parts:
                    part_index = index4 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
        elif frame_counter % 10 == 4:
            # draw enemy at frame 5
            if enemy.right:
                frame5 = enemy.frame5[1]
                index5 = enemy.frame5_indexes[1]
                enemy_parts = frame5.split('\n')
                for part in enemy_parts:
                    part_index = index5 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame5 = enemy.frame5[0]
                index5 = enemy.frame5_indexes[0]
                enemy_parts = frame5.split('\n')
                for part in enemy_parts:
                    part_index = index5 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
        elif frame_counter % 10 == 5:
            # draw enemy at frame 6
            if enemy.right:
                frame6 = enemy.frame6[1]
                index6 = enemy.frame6_indexes[1]
                enemy_parts = frame6.split('\n')
                for part in enemy_parts:
                    part_index = index6 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame6 = enemy.frame6[0]
                index6 = enemy.frame6_indexes[0]
                enemy_parts = frame6.split('\n')
                for part in enemy_parts:
                    part_index = index6 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
            hitCheck(enemy, player)
        elif frame_counter % 10 == 6:
            # draw enemy at frame 7
            if enemy.right:
                frame7 = enemy.frame7[1]
                index7 = enemy.frame7_indexes[1]
                enemy_parts = frame7.split('\n')
                for part in enemy_parts:
                    part_index = index7 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame7 = enemy.frame7[0]
                index7 = enemy.frame7_indexes[0]
                enemy_parts = frame7.split('\n')
                for part in enemy_parts:
                    part_index = index7 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
            hitCheck(enemy, player)
        elif frame_counter % 10 == 7:
            # draw enemy at frame 8
            if enemy.right:
                frame8 = enemy.frame8[1]
                index8 = enemy.frame8_indexes[1]
                enemy_parts = frame8.split('\n')
                for part in enemy_parts:
                    part_index = index8 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame8 = enemy.frame8[0]
                index8 = enemy.frame8_indexes[0]
                enemy_parts = frame8.split('\n')
                for part in enemy_parts:
                    part_index = index8 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
            hitCheck(enemy, player)
        elif frame_counter % 10 == 8:
            # draw enemy at frame 9
            if enemy.right:
                frame9 = enemy.frame9[1]
                index9 = enemy.frame9_indexes[1]
                enemy_parts = frame9.split('\n')
                for part in enemy_parts:
                    part_index = index9 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame9 = enemy.frame9[0]
                index9 = enemy.frame9_indexes[0]
                enemy_parts = frame9.split('\n')
                for part in enemy_parts:
                    part_index = index9 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
            hitCheck(enemy, player)
        elif frame_counter % 10 == 9:
            # draw enemy at frame 10
            if enemy.right:
                frame10 = enemy.frame10[1]
                index10 = enemy.frame10_indexes[1]
                enemy_parts = frame10.split('\n')
                for part in enemy_parts:
                    part_index = index10 + (enemy_parts.index(part) * level.w) + enemy_parts.index(part)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            else:
                frame10 = enemy.frame10[0]
                index10 = enemy.frame10_indexes[0]
                enemy_parts = frame10.split('\n')
                for part in enemy_parts:
                    part_index = index10 + (enemy_parts.index(part) * level.w)
                    for p in part:
                        p_index = part_index + part.index(p)
                        level_view[p_index] = p
            level_view = ''.join(level_view)
            hitCheck(enemy, player)
        else:
            # empty frame separating the start and the end of the animation loop
            level_view = level.setCanvas(frame_counter % 2)
            
        frame_counter += 1
        if frame_counter >= 11: #enemy.frame_count
            break

        # draw the enemy on the view
        level.canvas = level_view

        time.sleep(delay)

def updateEnemies(enemy, level, name, frame_delay, run_event, player):
    while run_event.is_set():
        # choose side for the enemy to spawn
        enemy.right = random.randint(0, 1)

        drawEnemy(enemy, level, frame_delay, player)

        

def draw(player, level):
    # player moving
    player.move()

    # drawing the car on the canvas
    canvas = level.canvas
    car_parts = []
    if player.right:
        car_parts = player.right_image.split('\n')
        for part in car_parts:
            part_index = ((7 + car_parts.index(part)) * level.w + level.h) + int(level.w / 2) + 2 - ((len(car_parts) - 1) - car_parts.index(part))
            canvas = list(canvas)
            for p in part:
                p_index = part_index + part.index(p)
                canvas[p_index] = p
            canvas = ''.join(canvas)
    else:
        car_parts = player.left_image.split('\n')
        for part in car_parts:
            part_index = ((7 + car_parts.index(part)) * level.w + level.h) + (int(level.w / 4) - car_parts.index(part) + 1) - ((len(car_parts)) - car_parts.index(part))
            canvas = list(canvas)
            for p in part:
                p_index = part_index + part.index(p)
                canvas[p_index] = p
            canvas = ''.join(canvas)

    # draw the view
    print(canvas)

    # clear the screen
    tmp = sp.call('cls', shell = True)

def play():
    level = Level(21, 10)
    player = Player(1)
    enemy = Enemy(10, 0, level)
    fps = 700
    time_delta = 1./fps

    # threading
    run_event = threading.Event()
    run_event.set()

    #making the thread variable global
    global thread1
    
    thread1 = Thread(1, "draw_enemy", updateEnemies, 1./8, run_event, level, enemy, player)

    # strating the threads
    thread1.start()

    try:
        while player.alive:
            draw(player, level)

            time.sleep(time_delta)
    except:
        print('\nExiting...')
        run_event.clear()
        thread1.join(timeout = 0)

# for testing
#play()