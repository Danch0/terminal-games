import time
import keyboard
import subprocess as sp

# global values
player_max_air_time = 3
player_max_y = 7

class Player:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.character = 'o'
        self.in_air = False
        self.air_time = player_max_air_time
        self.alive = True
        self.winner = False
        self.lifes = 1

    def move(self, level_map):
        # important values
        multiplier = player_max_y - self.y
        addition = multiplier + 1
        index = int(self.x + (multiplier * (len(level_map) - 8) / 8) + addition) # player x mapped to the map
        #print('NEXT CHAR: ' + level_map[index + 1])
        #print('PREV CHAR: ' + level_map[index - 1])

        # change x
        if keyboard.is_pressed('right'):
            if level_map[index + 1] != '|' and level_map[index + 1] != '/' and level_map[index + 1] != '\\' and level_map[index] != '/' and level_map[index] != '|' and level_map[index] != '\\':
                self.x += 1
        if keyboard.is_pressed('left'):
            if level_map[index - 1] != '|' and level_map[index - 1] != '/' and level_map[index - 1] != '\\' and level_map[index] != '/' and level_map[index] != '|' and level_map[index] != '\\':
                self.x -= 1
        
        # change y (jump)
        if keyboard.is_pressed('up') and self.air_time > 0:
            self.y += 1
            self.in_air = True
            self.air_time -= 1

        # in air check
        if not keyboard.is_pressed('up'):
            if level_map[index] == ' ':
                self.y -= 1
            else:
                self.air_time += 1
        else:
            if self.air_time == 0 and level_map[index] == ' ':
                self.y -= 1

        # check air time
        if self.air_time <= 0:
            self.air_time = 0
        if self.air_time >= player_max_air_time:
            self.air_time = player_max_air_time

        # check if x is in range
        map_layer_length = (len(level_map) / player_max_y + 1) - player_max_y + 1
        if self.x < 0:
            self.x = 0
        elif self.x >= map_layer_length:
            self.x = map_layer_length - 1

        # check if y is in range
        if self.y > player_max_y:
            self.y = player_max_y

class Level:
    def __init__(self):
        self.layer8 = '                                                                                       '
        self.layer7 = '                                                                                       '
        self.layer6 = '                                                                      ____             '
        self.layer5 = '                                  __+_                             __/\   |            '
        self.layer4 = '                          _____    \ \  ___    _______            /\_\_\__/\           '
        self.layer3 = '                   ____   |/  |     \ \_//    /\_____/\         _______________        '
        self.layer2 = '    _+__           | \/    \/ |      | \/     |    _  |       _/\  /    \    \/        '
        self.layer1 = '_____||__________^_|_|______|_|_^^_^_|__|_____|___|_|_|^^^___/\____\_________|__^_____#'
        self.map = '\n' + self.layer8 + '\n' + self.layer7 + '\n' + self.layer6  +'\n' + self.layer5 + '\n' + self.layer4 + '\n' + self.layer3 + '\n' + self.layer2 + '\n' + self.layer1

def draw(player, level):
    # player moving
    player.move(level.map)

    # print indexes
    print('X: ' + str(player.x))
    print('Y: ' + str(player.y))
    multiplier = player_max_y - player.y
    addition = multiplier + 1
    index = int(player.x + (multiplier * (len(level.map) - 8) / 8) + addition) # player x mapped to the map
    print('INDEX: ' + str(index))

    # jump check
    print(player.in_air)
    print('AIR TIME: ' + str(player.air_time))
    print('LIFES: ' + str(player.lifes))

    # check events
    getItem(level.map[index], player, level, index)
    death(level.map[index], player)
    win(level.map[index], player)

    # drawing the map
    drawn_map = level.map[:index] + player.character + level.map[index + 1:]
    print('\n\n\n\n' + drawn_map)

    # clear the screen
    tmp = sp.call('cls', shell = True)

def win(char, player):
    if char == '#':
        player.winner = True

def death(char, player):
    if char == '^':
        player.lifes -= 1
        if not player.lifes:
            player.alive = False

def getItem(char, player, level, item_index):
    if char == '+':
        player.lifes += 1
        level_map = list(level.map)
        level_map[item_index] = '_'
        level.map = ''.join(level_map)

def play():
    # init variables
    player = Player(0, 0)
    level = Level()
    fps = 1000
    time_delta = 1./fps

    while not player.winner and player.alive:
        draw(player, level)

        time.sleep(time_delta)

# for testing
#play()