import keyboard
import time
import subprocess as sp
import random
import threading

class Thread (threading.Thread):
    def __init__(self, threadID, name, func, delay, run_event, level):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.func = func
        self.delay = delay
        self.stop = False
        self.run_event = run_event
        self.level = level

    def run(self):
        #print("Starting " + self.name)
        self.func(self.level, self.name, self.delay, self.run_event)
        #print("Exiting " + self.name)

player_max_air_time = 4

class Player:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.in_air = False
        self.air_time = player_max_air_time
        self.alive = True
        self.character = 'o'

    def move(self, level):
        multiplier = level.h - 1 - self.y
        addition = multiplier + 1
        index = int(self.x + (multiplier * (len(level.map) - level.h) / level.h) + addition) # player x mapped to the map

        # change y (jump)
        if keyboard.is_pressed('up') and self.air_time > 0:
            self.y += 1
            self.in_air = True
            self.air_time -= 1

        # change x
        if keyboard.is_pressed('right'):
            self.x += 1
        if keyboard.is_pressed('left'):
            self.x -= 1

        # in air check
        if not keyboard.is_pressed('up'):
            if level.map[index] == ' ':
                self.y -= 1
            else:
                self.air_time += 1
        else:
            if self.air_time == 0 and level.map[index] == ' ':
                self.y -= 1

        # check air time
        if self.air_time <= 0:
            self.air_time = 0
        if self.air_time >= player_max_air_time:
            self.air_time = player_max_air_time

        # check if x is in range
        map_layer_length = (len(level.map) / level.h) - 1
        if self.x < 0:
            self.x = 0
        elif self.x >= map_layer_length:
            self.x = map_layer_length - 1

        # check if y is in range
        if self.y >= level.h:
            self.y = level.h - 1


class Level:
    def __init__(self, max_objects_on_screen, w, h):
        self.max_objects_on_screen = max_objects_on_screen
        self.ground = '_'
        self.scroll_speed = 1
        self.w = w
        self.h = h
        self.row = self.writeCleanRow()
        self.init_row = self.writeInitRow()
        self.map = self.writeMap()

    def writeCleanRow(self):
        char = ' '
        row = ''
        for c in range(self.w):
            row += char

        return row

    def writeInitRow(self):
        init_row = ''
        #char = self.ground
        char = '^'
        for c in range(self.w):
            init_row += char

        return init_row

    def writeMap(self):
        level_map = ''
        for c in range(self.h - 1):
            if c % 2 == 1: # WIP
                rand = random.randint(0, len(self.row) - 4)
                random_row = list(self.row)
                random_row[rand] = self.ground
                random_row[rand + 1] = self.ground
                random_row[rand + 2] = self.ground
                row = ''.join(random_row)
            else:
                row = self.row
            level_map += '\n' + row
        level_map += '\n' + self.init_row

        return level_map

def updateMap(level, name, delay, run_event):
    while run_event.is_set():
        # creating the new row
        level_map = level.map.split('\n')
        del level_map[0]
        del level_map[level.h - 2]
        #print(level_map)
        new_row = list(level.writeCleanRow())
        rand = random.randint(0, len(level.row) - 4)
        if random.randint(0, 7):
            new_row[rand] = level.ground
            new_row[rand + 1] = level.ground
            new_row[rand + 2] = level.ground
        new_row = ''.join(new_row)
        #print(new_row)
        new_level_map = '\n' + new_row

        # transfering the rows down
        for row in level_map:
            new_level_map += '\n' + row
        level_map = '\n'.join(level_map)
        #print(level_map)

        level.map = new_level_map

        time.sleep(delay)

def death(player, char, run_event, thread):
    if char == '^':
        player.alive = False

        # stopping the thread for map update
        #print('Exiting...')
        run_event.clear()
        thread.join(timeout = 0)

def draw(player, level, run_event, thread1):
    # player moving
    player.move(level)

    # print indexes
    print('X: ' + str(player.x))
    print('Y: ' + str(player.y))
    multiplier = level.h - 1 - player.y
    addition = multiplier + 1
    index = int(player.x + (multiplier * (len(level.map) - level.h) / level.h) + addition) # player x mapped to the map
    #print('INDEX: ' + str(index))

    # jump check
    print(player.in_air)
    print('AIR TIME: ' + str(player.air_time))

    # drawing the map
    drawn_map = level.map[:index] + player.character + level.map[index + 1:]
    #drawn_map = level.map
    print('\n\n' + drawn_map)

    # events
    death(player, level.map[index], run_event, thread1)

    # clear the screen
    tmp = sp.call('cls', shell = True)

def play():
    # init variables
    level = Level(5, 20, 10)
    player = Player(random.randint(0, level.w), level.h - 1)
    fps = 700
    time_delta = 1./fps
    
    # threading
    run_event = threading.Event()
    run_event.set()

    thread1 = Thread(1, "map_update", updateMap, 1./2, run_event, level)

    # strating the threads
    thread1.start()

    try:
        # main loop
        while player.alive:
            draw(player, level, run_event, thread1)

            time.sleep(time_delta)
    except:
        print('Exiting...')
        run_event.clear()
        thread1.join(timeout = 0)

# for testing
#play()